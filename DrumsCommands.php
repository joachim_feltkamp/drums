<?php

namespace Drush\Commands\drums;

use Drums\DrumsAliasCollection;
use Symfony\Component\Console\Application;
use Drush\Drush;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Consolidation\AnnotatedCommand\AnnotationData;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Terminal;

/**
 * Edit this file to reflect your organization's needs.
 */
class DrumsCommands extends DrushCommands {

  /**
   * The global options to be filtered.
   *
   * @var array
   */
  protected array $globalOptions = [];

  /**
   * @var \Drums\DrumsAliasCollection
   */
  protected DrumsAliasCollection $aliasCollection;

  /**
   * @var \Symfony\Component\Console\Application
   */
  protected Application $application;

  /**
   * @var int
   */
  private int $terminalWidth;

  public function __construct() {
    parent::__construct();
    $this->application = Drush::getApplication();
    // @ToDo Get Terminal width from application ?!!
    $this->terminalWidth = (new Terminal())->getWidth();
  }

  /**
   * Filter options from argument list and update command with named options.
   *
   * @hook option drums:multisite
   *
   * @ToDo Check if using $_Server['argv'] is a good practice.
   */
  public function additionalOption(Command $command, AnnotationData $annotationData) {
    ['global_options' => $global_options, 'shortcuts' => $shortcuts] = $this->getGlobalOptions();
    foreach ($_SERVER['argv'] as $key => $option) {
      if (preg_match('/--(\w[-\w]*)(=.*)?/', $option, $matches)) {
        if (!in_array($matches[1], $global_options)) {
          $command->addOption(
            $matches[1],
            '',
            (count($matches) >= 3) ? InputOption::VALUE_OPTIONAL : InputOption::VALUE_NONE,
            'Option added dynamic by @hook option drums:multisite'
          );
        }
      } elseif (preg_match('/-([a-zA-Z]*)/', $option, $matches)) {
        if (!in_array($matches[1], $shortcuts)) {
          throw new \Exception("Command drums:multisite does not support shortcuts as '-{$matches[1]}' for subcommands. Please use explicit options with double dash.");
        }
      }
    }
  }

  /**
   * Prepare params and
   *
   * @hook pre-init @drums_multisite_args
   */
  public function preInit(InputInterface $input, AnnotationData $annotationData) {
    $params = [];
    $cmd = NULL;
    $distributor = NULL;
    $x = 0;
    while ($input->hasArgument("param_$x")) {
      $param = $input->getArgument("param_$x");
      if ($param && !$distributor && str_starts_with($param, '@@')) {
        $distributor = $param;
      } elseif($param && !$cmd) {
        $cmd = $param;
      } else {
        $input->setArgument("param_$x", $param);
        if ($param) { $params[] = $param; }
      }
      $x++;
    }
    $input->setArgument("cmd", $cmd);
    $input->setArgument('distributor', $distributor ?? '@@local');
    $input->setArgument("params", $params);
  }

  /**
   * Drush Multisite (Drums) helps administrate Drupal multisite installations.
   *
   * Defined drush aliases for local and remote sites are required.
   *
   * @usage <fg=green;options=bold>drush ms cr</>
   *   Clear cache for all local sites.
   * @usage <fg=green;options=bold>drush ms @@remote cr</>
   *   Clear cache for all remote sites.
   * @usage <fg=green;options=bold>drush ms @@*.prod status</>
   *   Get drush core:status for productive remote hosts.
   * @usage <fg=green;options=bold>drush ms @@mysite.* status</>
   *   Get drush status for site mysite from all envs (e.g. dev, stage, prod).
   * @usage <fg=green;options=bold>drush ms @@*.prod#dev:%files/img/ rsync</>
   *   Same as:
   * @usage <fg=green;options=bold>:-)</>
   * drush rsync @mysite.prod:%files/img/ @mysite.dev:%files/img/
   * @usage <fg=green;options=bold>:-))</>
   *   drush rsync @yoursite.prod:%files/img/ @yoursite.dev:%files/img/
   * @usage <fg=green;options=bold>:-)))</>
   *   drush rsync @oursite.prod:%files/img/ @oursite.dev:%files/img/
   *
   * @command drums:multisite
   * @aliases ms
   *
   * @drums_multisite_args
   *
   * @param string $param_0 Optional parameter 0 passed through to subcommand.
   * @param string $param_1 Optional parameter 1 passed through to subcommand.
   * @param string $param_2 Optional parameter 2 passed through to subcommand.
   * @param string $param_3 Optional parameter 3 passed through to subcommand.
   * @param string $param_4 Optional parameter 4 passed through to subcommand.
   * @param string $param_5 Optional parameter 5 passed through to subcommand.
   * @param string $param_6 Optional parameter 6 passed through to subcommand.
   * @param string $param_7 Optional parameter 7 passed through to subcommand.
   * @param string $param_8 Optional parameter 8 passed through to subcommand.
   * @param string $param_9 Optional parameter 9 passed through to subcommand.
   * @param string $cmd The drush subcommand to be executed for multiple sites.
   * @param array $params The parameter collection.
   * @param array $distributor The alias selector collection.
   *
   * @option dry-run Optional
   *
   */
  public function drumsMultisite(InputInterface $input)
  {
    // Get basic params.
    $query = $input->getArgument('distributor');
    $cmd = $input->getArgument('cmd');
    $params = $input->getArgument('params');

    // Build alias item collection to work on.
    $this->aliasCollection = new DrumsAliasCollection($query, $this->io());

    // Get further arguments to pass to subcommand.
    $arguments = $input->getArgument('params');

    // Get options with global options filtered out.
    $options = $input->getOptions();
    ['global_options' => $global_options] = $this->getGlobalOptions();
    $options = array_filter($input->getOptions(), function ($opt) use ($options, $global_options) {
      return !in_array($opt, $global_options) && !!$options[$opt];
    },  ARRAY_FILTER_USE_KEY);

    // Build Arguments and options string for sub-command.
    $arg_opt_str = $this->getArgOptsString($arguments, $options);

    // Build result table of alias query.
    [$table_head, $table_body] = $this->aliasCollection->getTable();
    $this->io()->table($table_head, $table_body);

    // Build command.
    if ($this->aliasCollection->getType() == DrumsAliasCollection::SOURCE_TARGET) {
      $command = "drush $cmd @_SOURCE @_TARGET $arg_opt_str";
    } elseif($this->aliasCollection->getType() == DrumsAliasCollection::SINGLE_SITE) {
      $command = "drush @_ALIAS $cmd $arg_opt_str";
    }

    $this->drumsExec($command, !!$input->getOption('dry-run'));
  }

  /**
   * @command drums:shell
   * @aliases msh
   *
   * @drums_multisite_args
   *
   * @param string $param_0 Optional parameter 0 passed through to subcommand.
   * @param string $param_1 Optional parameter 1 passed through to subcommand.
   * @param string $param_2 Optional parameter 2 passed through to subcommand.
   * @param string $param_3 Optional parameter 3 passed through to subcommand.
   * @param string $param_4 Optional parameter 4 passed through to subcommand.
   * @param string $param_5 Optional parameter 5 passed through to subcommand.
   * @param string $cmd The drush subcommand to be executed for multiple sites.
   * @param array $params The parameter collection.
   * @param array $distributor The alias selector collection.
   *
   * @option dry-run Optional
   *
   * @throws \Exception
   */
  public function drumsShell(InputInterface $input) {
    $this->aliasCollection = new DrumsAliasCollection($input->getArgument('distributor'), $this->io());

    [$table_head, $table_body] = $this->aliasCollection->getTable(['type' => 'wd', 'default_cwd' => '%site/']);
    $this->io()->table($table_head, $table_body);

    $command = "drush @_ALIAS ssh \"{$input->getArgument('cmd')}\" --cd=@_CWD";
    $this->drumsExec($command, !!$input->getOption('dry-run'), 'site');
  }

  /**
   * @param string $command
   *   Command to execute with placeholders for alias specific frags.
   * @param bool $dry_run
   *   Simulate execution by displaying the final formatted command.
   * @param string|null $cwd_default
   *   The default working directory.
   *
   * @return void
   *
   * @throws \Exception
   */
  private function drumsExec(string $command, bool $dry_run = TRUE, string $cwd_default = NULL): void {
    $msg_params = ['!exec'=> ($dry_run) ?'Dry run' :'Execute', '!command' => $command];
    if($this->io()->confirm(dt('!exec <fg=cyan>"!command"</> for all listed aliases?', $msg_params))) {
      foreach ($this->aliasCollection as $alias) {
        $cmd = str_replace(['@_SOURCE @_TARGET', '@_ALIAS'], $alias->getArgumentString(), $command);
        if ($cwd = $alias->getCwd($cwd_default)) {
          $cmd = str_replace(['@_CWD'], $cwd, $cmd);
        }
        if ($dry_run) {
          $this->io()->writeln(dt("<fg=green>Dry run</> $ <fg=cyan>!cmd</>", ['!cmd' => $cmd]));
        } else {
          try {
            $process = $this->processManager()->shell($cmd);
            $result = $process->mustRun();
            $cmd_feedback = $result->getOutput() ?: $result->getErrorOutput();
            $success = $process->isSuccessful();
          } catch (\Exception $e) {
            $cmd_feedback = $e->getMessage();
            $success = FALSE;
          }

          $cmd_feedback = $this->textAddLineWraps($cmd_feedback);

          $style = ($success) ? "<fg=default>": "<fg=red>";
          $success_msg = ($success) ? "Okay": "ERROR";
          $this->io()->table(["\u{1F941} Output <fg=cyan>$cmd</>", "Result"],
            [
              ["$style$cmd_feedback</>", "$style$success_msg</>"],
            ]
          );
        }
      }
    }
  }

  /**
   * Returns an assoc array with all global options and their shortcuts.
   *
   * @return array|array[]
   */
  protected function getGlobalOptions(): array {
    if(!count($this->globalOptions)) {
      // Array with predefined options used by drums:multisite.
      $cont = [
        'global_options' => ['dry-run'],
        'shortcuts' => ['v', 'vv', 'vvv'],
      ];
      $def = $this->application->getDefinition();
      foreach ($def->getOptions() as $key => $value) {
        $cont['global_options'][] = $key;
        if ($sc = $value->getShortcut()) {
          $cont['shortcuts'][] = $sc;
        }
      }
      $this->globalOptions = $cont;
    }
    return $this->globalOptions;
  }

  /**
   * @param array $arguments
   * @param array $options
   *
   * @return string
   */
  protected function getArgOptsString(array $arguments = [], array $options = []): string
  {
    $argsStr = implode(' ', $arguments);
    $optsStr = '';
    foreach($options as $key => $val) {
      switch (gettype($val)) {
        case 'string':
        case 'integer':
          $optsStr .= " --$key=$val";
          break;
        case 'boolean':
          if($val) { $optsStr .= " --$key"; }
          break;
        default:
      }
    }
    return trim("$argsStr$optsStr");
  }

  /**
   * @param string $text
   * @param int $reducer
   *
   * @return string
   */
  protected function textAddLineWraps(string $text, int $reducer = 20): string {
    $length = $this->terminalWidth - $reducer;
    return wordwrap(trim($text), $length);
  }

}
