<?php
namespace Drush\Commands\drums;

use Drums\DrumsFileManager;
use DrupalFinder\DrupalFinder;
use Drush\Commands\DrushCommands;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;
use Symfony\Component\Yaml\Yaml;


class DrumsSetupCommands extends DrushCommands implements CustomEventAwareInterface {

  use CustomEventAwareTrait;

  /**
   * @var \Drums\DrumsFileManager|null
   */
  private ?DrumsFileManager $fileManager;

  /**
   * @var \DrupalFinder\DrupalFinder|null
   */
  private ?DrupalFinder $drupalFinder;

  /**
   * @var Finder|null
   */
  private ?Finder $finder;

  /**
   * @var Filesystem|null
   */
  private ?Filesystem $fs;

  /**
   * @var array
   */
  private array $sites = [];

  /**
   * @var string
   */
  private string $drupalRoot;

  /**
   * @var string
   */
  private string $composerRoot;

  public function __construct() {
    parent::__construct();
    $this->fileManager = new DrumsFileManager();
    $this->finder = new Finder();
    $this->fs = new Filesystem();
    $this->drupalFinder = new DrupalFinder();
    $this->drupalFinder->locateRoot(getcwd());
    $this->drupalRoot = $this->drupalFinder->getDrupalRoot();
    $this->composerRoot = $this->drupalFinder->getComposerRoot();
  }

  /**
   * Drush Multisite (Drums) helps administrate Drupal multisite installations.
   *
   *
   * @command drums:setup
   *
   */
  public function drumsSetup(InputInterface $input) {

    $drumsConfigs = $this->fileManager
      ->findFiles('sites', '/^drums\.y(a)?ml$/', '== 1', TRUE);

    foreach ($drumsConfigs as $path => $siteConfig) {
      try {
        $site = Yaml::parse($siteConfig->getContents());
        $siteId = (preg_match('/([^\/]+)\/drums\.ya?ml$/', $path, $matches))
          ? $matches[1] : $site['siteid'];
        if (!$siteId) throw new \Exception("No site id found.");
        $this->sites[$siteId] = $site;
      } catch (\Exception $exception) {
        $this->io()->error('Unable to parse the YAML string: '. $exception->getMessage());
      }
    };

    $setupServices = [];
    $handlers = $this->getCustomEventHandlers('drums-setup-services');
    foreach ($handlers as $handler) {
      $handlerResult = $handler($this->sites);
      $setupServices = array_merge($setupServices, $handlerResult);
    }

    $this->io()->table(['File', 'Operation', 'Post command'], $setupServices);
    $this->io()->writeln(	"\u{1F941} DRUMS setup completed.");
  }

  /**
   * Creates/updates site aliases.
   *
   * @hook on-event drums-setup-services
   */
  public function createAliasesService(array $sites)
  {
    $drushAliasDir = "drush/sites";
    $response = [];
    // Prepare directory.
    $this->fileManager->prepareFolder($drushAliasDir);

    // Get existing alias file names, to know what files exist and what files
    // have to be deleted after the update process.
    $aliases = $this->fileManager
      ->findFiles($drushAliasDir, '/^[^\.]+\.site\.y(a)?ml$/', 0);
    $aliasFileNames = [];
    foreach ($aliases as $alias) { $aliasFileNames[] = $alias->getFilename(); };

    foreach ($sites as $siteId => $config) {
      $enabled = $config['enabled'] ?? TRUE;
      $drushAlias = $config['drushalias'] ?? FALSE;
      if ($enabled && $drushAlias) {
        $yaml = Yaml::dump($drushAlias);
        $yamlPath = "$drushAliasDir/$siteId.site.yml";

        // Reduce list by updated file name.
        $aliasFileNames = array_filter(
          $aliasFileNames, function($a) use ($siteId) {
            return $a != "$siteId.site.yml";
          });

        // Create alias file.
        $fileComment = "# File managed by Drums."
          . "\n# Your manual changes will be overwritten.\n\n";
        $response[] = $this->fileManager
          ->setFileContent($yamlPath, $fileComment.$yaml);
      }
    }

    // Remove the rest of the pre-existing aliases.
    foreach ($aliasFileNames as $aliasFileName) {
      $response[] = $this->fileManager->removeFile("$drushAliasDir/$aliasFileName", []);
    }

    return $response;
  }


  /**
   * Creates/updates the sites.php .
   *
   * @hook on-event drums-setup-services
   */
  public function createSitesPhpService(array $sites): array {
    $siteMap = [];
    foreach ($sites as $siteId => $config) {
      $enabled = $config['enabled'] ?? TRUE;
      if ($enabled) {
        $siteVhosts = $config['vhosts'] ?? [];
        foreach($siteVhosts as $vhost) {
          $siteMap[$vhost] = $siteId;
        }
      }
    }
    $sitePhpArr = var_export($siteMap, true);
    $fileComment = "\n# File managed by Drums."
      . "\n# Your manual changes will be overwritten.\n\n";
    $sitePhpCode = "<?php{$fileComment}\n" . '$sites' ." = $sitePhpArr;\n";
    $result = $this->fileManager
      ->setFileContent("sites/sites.php", $sitePhpCode, [], TRUE);
    return [$result];
  }
}
