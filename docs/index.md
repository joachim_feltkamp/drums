# 🥁 DRUMS Einführung

## Was ist DRUMS?

Das Wort "Drums" [drʌms] ist ein Acronym für **Dru**sh **M**ulti-**S**ite.

Die Drums Software besteht in einer Erweiterung des Kommandozeilentools **Drush** (**Dru**pal **Sh**ell), mit dem wiederum Installationen des Open-Source CMS **Drupal** über die Kommandozeile administriert werden.

Drums erweitert im Kontext einer Drupal-Multisite-Installation den Funktionsumfang von Drush in der Weise, dass <u>Drush-Commands</u> nicht nur für *eine* - sondern gleich <u>für mehrere oder alle Websites</u> ausgeführt werden können.

### Voraussetzungen

Voraussetzung dafür, dass Drums eine sinnvolle Ergänzung für Sie darstellt, ist, dass Sie

1. eine Drupal (Version 8, 9, 10) Multi-Site-Installation betreiben (wollen).
1. mit dem Drush Kommandozeilen-Tool (Version 10, 11, 12) vertraut sind.
1. dass Sie mit Drush-Aliasen vertraut sind.
1. ... und nach einer Möglichkeit suchen, die Administration einer Drupal-Multisite zu vereinfachen.

Wenn Sie jeden dieser Punkte verstanden haben und bestätigen, dann können Die die direkt

[weiter springen zu "Und was ist nun Drums?"](#finale-und-was-ist-nun-drums){ .md-button .md-button--primary .centered }

### Was ist Drush?

Drush ist ein umfangreiches Kommandozeilen-Tool, mit dem man Drupal Websites verwalten kann. Jeder Drupal-Entwickler kennt und nutzt es. ([Mehr Informationen](https://www.drush.org/latest/){:target="_blank"})

Mit **Drush** kann man z.B. über eine Kommandozeile den Cache einer Drupal-Site löschen:

``` shell
$ drush cr
```

### Was sind Drush-Aliase?

Eine Drupal-Site hat meistens mehrere Environments: Development, Staging und Production, welche zum Teil lokal auf dem Computer (Development) und auf entfernten Servern (Staging, Production) installiert sind.

Drush bietet mit dem großartigen **Konzept der Drush-Aliase** die Möglichkeit, mit dem CLI sowohl auf lokale, als auch auf entfernte Environments zuzugreifen, ohne dass ein Developer selbst eine SSH-Verbindung herstellen muss.[^1]

[^1]: Wie Aliase eingerichtet und verwendet werden können, ist alles genau in der [Drush Dokumentation](https://www.drush.org/latest/site-aliases/){:target="_blank"} beschrieben.

``` shell title='Bsp: Cache auf einem entfernten Server löschen'
$ drush @staging cr
```

Darüber hinaus können durch __Drush-Aliase__ die verschiedenen Environments __Daten miteinander austauschen__.

``` shell title='Bsp: Aktuellen Datenbank-Dump von Production auf Development importieren.'
$ drush sql:sync @production @development
```
### Was ist Drupal-Multisite?

Das Drupal-CMS ist "mandantenfähig"[^2]. Das bedeutet, dass mehrere Websites in einer einzigen Installation von Drupal betrieben werden können. Die Websites verwenden i.d.R. __denselben Code__ (Symfony, Drupal-Core, Drupal-Contrib-Module und auch Custom-Module), aber __verschiedene Datenbanken__, was Vorteile bezüglich der Code-Wartung hat, aber auch einige neue Probleme mit sich bringt (z.B. beim Deployment von Code-Updates).

!!! info "Zum Verständnis von *Drupal Multisite*"

    Mandantenfähigkeit bedeutet nicht, dass **eine Website** auf **mehreren Environments** betrieben wird, sondern dass in **einem bzw. jedem Environment** (z.B. Production) **mehrere Websites** betrieben werden, die je verschiedene Hostsnamen, verschiedenen Content und verschiedene Konfiguration verwenden.

    Wenn dieses Konzept nicht vertraut ist, finden Sie hier nähere Information dazu: [Drupal.org - Multisite](https://www.drupal.org/docs/multisite-drupal/set-up-a-multisite).

[^2]: Als mandantenfähig (auch mandantentauglich) wird Informationstechnik bezeichnet, die auf demselben Server oder demselben Software-System mehrere Mandanten, also Kunden oder Auftraggeber, bedienen kann, ohne dass diese gegenseitigen Einblick in ihre Daten, Benutzerverwaltung und Ähnliches haben. [Quelle: Wikipedia](https://de.wikipedia.org/wiki/Mandantenf%C3%A4higkeit)

Eine Drupal-Multisite-Installation ist sehr einfach aufsetzbar. In einer Drupal-Multisite-Installation beherbergt der Ordner ```<docroot>/sites``` eine Datei ```sites.php``` (welche ein Mapping-Array enthält, welcher Host-Name mit welcher Site verknüpft ist) und für jede Website einen Ordner (welcher nach der Site-ID benannt ist und dessen gesamten individuellen Code beinhaltet, wie z.B. die ```settings.php```). Alles Weitere verhält sich genau so, wie in einer Single-Site-Installation.

### Multisite-Aliase

Auch __Drupal-Multisite__ wird mit __Drush-Aliassen__ unterstützt. Aliase in einem Multisite-Kontext bestehen aus zwei Teilen: Site-ID und Environment-ID. Die __Site-ID__ wird vor allem durch den Namen des __Site-Ordners__ festgelegt, der sich unter dem Pfad ```<docroot>/sites/<site-id>/``` befindet. Die Environment-ID wird durch die Alias-Definition in der Datei ```/drush/sites/<site-id>.site.yml``` befindet.

#### Alias-Übersicht

|    Host/Domain    |    Site-Id    |  Development   |     Staging      |   Production    |
|:-----------------:|:-------------:|:--------------:|:----------------:|:---------------:|
|  **my-site.com**  |  **my-site**  |  @my-site.dev  |  @my-site.stage  |  @my-site.prod  |
| **your-site.com** | **your-site** | @your-site.dev | @your-site.stage | @your-site.prod |
| **bobs-site.org** | **bobs-site** | @bobs-site.dev | @bobs-site.stage | @bobs-site.prod |
| **kims-site.net** | **kims-site** | @kims-site.dev | @kims-site.stage | @kims-site.prod |



``` shell title='Bsp: Drush-Status von verschiedenen Sites auf verschiedenen Environments.'
$ drush @my-site.prod status
$ drush @my-site.dev status
$ drush @kims-site.dev status
```

### **Finale:** Und was ist nun Drums?

Drums bietet die Möglichkeit, einen Drush-Command für eine bestimmte Gruppe oder alle Aliase auszuführen, ohne dass es dafür erforderlich ist, alle Aliase genau zu kennen.

Drums bietet im wesentlichen zwei Commands an.

#### 1. Drush-Befehle für mehrere Aliase ausführen.

``` shell title="Command drums:multisite"
$ drush drums:multisite <distributor> <sub-command>
// Oder in der Kurzschreibweise
$ drush ms <distributor> <sub-command>
```
Der Command erwartet 2 Parameter.

**distributor:** Ist eine abstrakte Alias-Notation, welche es erlaubt, Wildcards (*) zu verwenden. (z.B. ```@@*.dev``` oder ```@@*.*```)

**sub-command:** Ist der Drush-Command, der für alle Aliase ausgeführt werden soll, die mit dem distributor matchen.

``` shell title="Bsp: Cache-Rebuild auf allen Seiten im DEV-Environmet."
// Mit DRUMS
$ drush ms @@*.dev cache:rebuild

// Führt folgende Commands aus:
$ drush @my-site.dev cache:rebuild
$ drush @your-site.dev cache:rebuild
$ drush @bobs-site.dev cache:rebuild
$ drush @kims-site.dev cache:rebuild
```


#### 2. Andere Befehle (git, composer, ls, ...) für mehrere Aliase ausführen.

``` shell title="Command drums:shell"
$ drush drums:shell <distributor> "<command>"
// Oder in der Kurzschreibweise
$ drush msh <distributor> "<command>"
```

**distributor:** Ist eine abstrakte Alias-Notation, welche es erlaubt, Wildcards (*) zu verwenden. Außerdem kann eine Pfad-Erweiterung angefügt werden, welche angibt, in welchem Verzeichnis der Command ausgeführt werden soll. (z.B. ```@@*.dev:%site/``` oder ```@@*.dev:%site/files```) Vorausgesetzt, dass Pfad-Platzhalter (z.B. %site, %private, %files, %config) im Alias-File definiert wurden.

**sub-command:** Ist ein Command-String, der für alle Aliase ausgeführt werden soll, die mit dem distributor matchen.

``` shell title="Bsp: Git-Pull im Config-Sync Folder der Site ausführen."
// Mit Drums
$ drush msh @@*.prod:%config/sync "git pull"

// Führt folgende Commands aus:
$ drush @my-site.prod ssh "cd /path/to/web/sites/my-site/config/sync && git pull"
$ drush @your-site.prod ssh "cd /path/to/web/sites/your-site/config/sync && git pull"
$ drush @bobs-site.prod ssh "cd /path/to/web/sites/bobs-site/config/sync && git pull"
$ drush @kims-site.prod ssh "cd /path/to/web/sites/kims-site/config/sync && git pull"
```

## Vorteile von Drums

* Vereinfachung der Administration einer Drupal-Multisite-Installation
* Zeitersparnis und Übersichtlichkeit
* Vereinfachung von Deployment-Prozessen von Drupal-Multisite-Installationen

## Verfügbarkeit

!!! success "Drums Maintainer Joachim Feltkamp says: "

    This project is under active development.
