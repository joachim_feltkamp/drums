# Install DRUMS in your Multisite project

DRUMS is a normal composer package and can easily be installed by composer. But it needs to be __installed in a special place__ so that drush will find the DRUMS-commands as required.

You will have to __edit your composer.json__ so that this will be considered during a composer install or update. Make sure that your composer properties contains at least these settings for ```extra.installer-types``` and ```extra.installer-paths```:

```json title="composer.json"
{
  "extra": {
    "installer-types": [
      "...",
      "drupal-drush"
    ],
    "installer-paths": {
      "drush/Commands/contrib/{$name}": [
        "type:drupal-drush"
      ]
    }
  }
}

```

With that you can require the DRUMS-package as normal with composer.

```shell
$ composer require joachim_feltkamp/drums
```

!!! info "DRUMS commands are plug&play available"

    The DRUMS-commands ```drums:multisite``` and ```drums:shell``` are "side-wide drush commands". This means they are not installed as normal drush commands in a module. Once the DRUMS package is installed, you can use the commands without enabling any module or smth.
    ([More about side-wide commands](https://www.drush.org/latest/commands/#site-wide-drush-commands){:target="_blank"})

## Prerequisites

DRUMS comes with some requirements, and maybe you have some version conflicts.


```json title="drums/composer.json"
{
    "require": {
        "php": ">= 7.4",
        "composer-runtime-api": "^2.2.2",
        "consolidation/site-alias": "^3.1",
        "drush/drush": "^10.0 || ^11.0 || ^12.0",
        "symfony/filesystem": "^3.4|^4.0|^5.0",
        "symfony/event-dispatcher": "^3.4|^4.0|^5.0",
        "webflo/drupal-finder": "^1.2"
    }
}
```
This is hard to maintain because we cannot test on such big variety of combinations and mostly work in a fully updated environment. So we **please document these issues** in the issue cue.
