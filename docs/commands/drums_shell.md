# drums:shell

Führt einen beliebigen Shell-Command in Site-spezifischen Ordnern aus - für mehrere oder alle verfügbaren Aliasse, wenn diese mit dem Alias-Query matchen. Wird kein Alias-Query angegeben, wird der Shell-Command für alle lokalen Sites ausgeführt.

**Kurzform:** ``msh``

```shell title="Struktur"
$ drush drums:shell [alias-query] "<sub-command>"
```

## Argumente

- **alias-query:** (default: `@@local`) Das erste Argument, das mit @@ beginnt, ist der Alias-Query, welcher bestimmt, für welche Aliasse der Sub-Command ausgeführt wird. Der Alias-Query muss eine Pfad-Erweiterung enthalten. [Mehr zur Form des Alias-Query](/work_with_drums/alias-queries/).
- **sub-command:** Das erste Argument, das nicht mit @@ beginnt, ist der Sub-Command, also ein beliebiger Shell-Command. Sofern der Sub-Command Leerzeichen bzw. eigene Argumente enthält, wird der Sub-Command mit Quotes ``"`` umschlossen.

##Optionen
- ``--dry-run`` - Statt den Sub-Command für alles Aliasse auszuführen, wird eine Liste aller Befehle in der ausführlichen Form mit ersetzten Platzhaltern ausgegeben.

#### Globale Optionen

- ``--yes|-y`` - Vor der Ausführung eine drums:shell Commands wird ein Bestätigungsdialog angezeigt, welcher den Command-String mit Platzhaltern ausgibt und außerdem eine Tabelle mit den Values, welche die Platzhalter iterativ ersetzen. Wird die Option `--yes` gesetzt, wird dieser Dialog bestätigt und übersprungen. (Diese Option ist global, d.h. wenn die Option gesetzt ist, dann gilt sie auch für weitere Bestätigungsdialoge.)
- ```-v|vv|vvv --verbose``` - Erhöhen Sie die Ausführlichkeit von Rückmeldungen: -v für normale Ausgabe, -vv für ausführlichere Ausgabe und -vvv für Debug.

!!!warning "Vorsicht mit der --yes option"

    Die `--yes`-Option ist für den **Gebrauch in Automatisierungs-Scripten**. Wenn Sie selbst unmittelbar mit dem CLI arbeiten, sollten Sie die Dialoge immer dazu nutzen, sich selbst noch einmal zu prüfen, um **mögliche Verluste** zu vermeiden.

    **Empfehlung:** Beim arbeiten mit dem CLI **auf Gebrauch der `--yes` Option zu verzichten**.


## Beispiele

- ``drush msh @local:%files pwd`` - Gibt für alle lokalen Sites den absolute Pfad zu ihrem jeweiligen `files`-Folder aus.
- ``drush msh @@*.stage:%site "git pull"`` - Führt einen `git pull` Command im Sites-Folder aller Sites im ``stage``-Environment aus (z.B. für `@my-site.stage`, `@your-site.stage`, ...).

