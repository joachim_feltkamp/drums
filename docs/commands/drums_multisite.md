# drums:multisite

Führt einen beliebigen Drush-Command für mehrere oder alle verfügbaren Aliasse aus, wenn diese mit dem Alias-Query matchen. Wird kein Alias-Query angegeben, wird der Drush-Command für alle lokalen Sites ausgeführt.

**Kurzform:** ``ms``

```shell title="Struktur"
$ drush drums:multisite [alias-query] <sub-command> <sub-command-arguments>
```

## Argumente

Der Command akzeptiert bis zu 10 Argumente, die jedoch größtenteils an den Sub-Command weitergereicht werden.

- **alias-query:** (default: `@@local`) Das erste Argument, das mit @@ beginnt, ist der Alias-Query, welcher bestimmt, für welche Aliasse der Sub-Command ausgeführt wird. [Weitere Informationen zu Form des Alias-Query](/work_with_drums/alias-queries/).
- **sub-command:** Das erste Argument, das nicht mit @@ beginnt, ist der Sub-Command, also ein beliebiger anderer Drush-Command.
- Alle weiteren Argument werden an den Subcommand weitergereicht.

##Optionen
- ``--dry-run`` - Statt den Sub-Command für alles Aliasse auszuführen, wird eine Liste aller Befehle in der ausführlichen Form mit ersetzten Platzhaltern ausgegeben.

#### Globale Optionen

- ``--yes|-y`` - Vor der Ausführung eine drums:multisite Commands wird ein Bestätigungsdialog gestartet, welcher den Command-String mit Platzhaltern ausgibt und außerdem eine Tabelle mit den Values, welche die Platzhalter iterativ ersetzen. Wird die Option gesetzt, wird dieser Dialog bestätigt und übersprungen. (Diese Option ist global, d.h. wenn die Option gesetzt ist, dann gilt sie auch für weitere Bestätigungsdialoge.)
- ```-v|vv|vvv --verbose``` - Erhöhen Sie die Ausführlichkeit von Rückmeldungen: -v für normale Ausgabe, -vv für ausführlichere Ausgabe und -vvv für Debug.

!!!warning "Vorsicht mit der --yes option"

    Die `--yes`-Option ist für den **Gebrauch in Automatisierungs-Scripten**. Wenn Sie selbst unmittelbar mit dem CLI arbeiten, sollten Sie die Dialoge immer dazu nutzen, sich selbst noch einmal zu prüfen, um **mögliche Verluste** zu vermeiden.

    **Empfehlung:** Beim arbeiten mit dem CLI **auf Gebrauch der `--yes` Option zu verzichten**.


## Beispiele

- ``drush ms cr`` - Führt einen cache-rebuild auf allen lokalen Sites aus.
- ``drush ms @@*.stage updb`` - Führt ein Datenbank-Update auf allen Sites im ``stage``-Environment aus (z.B. für `@my-site.stage`, `@your-site.stage`, ...).
- ``drush ms @@*.prod#dev sql:sync`` - Synchronisiert die Datenbanken aller Sites, die ein ``prod``- und ein ``dev``-Environment haben, von ``prod`` (Source) nach ``dev`` (Target).
- ``drush ms @@*.prod#dev:%files/images/ core:rsync`` - Synchronisiert den Ordner ``%files/images`` aller Sites, die ein ``prod``- und ein ``dev``-Environment haben, von ``prod`` (Source) nach ``dev`` (Target).

