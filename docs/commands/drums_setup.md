# drums:setup

Konfiguriert das Multisite-Environment für alle Sites, die in diesem Environment leben.

**Wenn Sie das Multisite-Environment-Konzept von DRUMS verwenden,** sollte dieser Command zumindest immer dann ausgeführt werden, wenn Sie eine Site in ein Environment einfügen oder entfernen, oder Änderungen an einer `drums.yml` im Site-Folder einer Site vorgenommen habe.

Der Command ist dafür vorgesehen, **alle Änderungen im File-System** vorzunehmen, die erforderlich sind, um Sites im Kontext einer Multisite-Installation lauffähig zu machen. Es ist z.B. nicht möglich, an dieser Stelle schon eine Datenbank zu importieren, weil z.B. bei einer neuen Site in diesem Kontext der Alias noch nicht gesetzt ist.

## Beispiel

- ``drush drums:setup``

(Der Command hat keine weiteren Argumente und Optionen.)

## Folgende Aktionen werden standardmäßig ausgeführt:

1. Erzeugen der Drush-Alias-Config `<root>/drush/sites/<site-id>.site.yml` aus der Config in `<root>/<docroot>/sites/<site-id>/drums.yml`.
2. Erzeugen der Datei `<root>/<docroot>/sites/site.php` aus der Config in `<root>/<docroot>/sites/<site-id>/drums.yml`.

## Weiterer Nutzen

Der drums:setup Command ruft Drush-Hooks vom Typ `@drums-setup-services` auf, welche weitere spezifische Anpassungen einer Site an Ihr Setup vornehmen können. Damit können Sie z.B. Verzeichnisse erzeugen, die Sie benötigen (z.B. `private`, `backup` und `tmp` folder) oder Config-Dateien für Ihre Docker-Umgebung.

Das folgende Template wird Ihnen dabei nützlich sein, eigene Hooks zu entwickeln, um ihr Setup zu automatisieren. Platzieren Sie die folgende Datei im Root-Verzeichnis ihres Projektes unter dem angegeben Pfad.

```php title='Place file here: /drush/Commands/custom/my_setup/MySetupCommands.php' linenums="1"
<?php
namespace Drush\Commands\my_setup;

use Drush\Commands\DrushCommands;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareInterface;
use Consolidation\AnnotatedCommand\Events\CustomEventAwareTrait;

class MySetupCommands extends DrushCommands implements CustomEventAwareInterface {

  use CustomEventAwareTrait;

  /**
   * Creates a file e.g. .ddev/config.drums.yml
   *
   * @hook on-event drums-setup-services
   */
  public function createDdevConfigService(array $sites): array
  {
    $summary = [];
    foreach($sites as $site_id => $site_drums_config_yml) {
      // Do stuff here e.g. to create config for an additional host name.

      $summary[] =  [
        'uri' => '/.ddev/config.drums.yml', // changed file uri.
        'status' => 'create', // update/delete/skip
        'post_cmd' => 'ddev restart', // Required command to let changes take effect.
      ];
    }

    // Example feedback for summary.
    return $summary;
  }
}
```

!!!info "Nutzen Sie die drums.yml für Site-spezifische Konfiguration"

    Wenn Sie zusätzliche Site-spezifische Konfiguration benötigen, um daraus Dateien im Environment zu erzeugen, können Sie dafür die `drums.yml` nutzen, die in in jedem Site-Folder unter `<docroot>/sites/<site-id>/drums.yml` liegt. Die Methode Ihres Drush-Hooks bekommt als Parameter ein Array, in dem die geparsten YAML-Dateien aller Sites enthalten sind.

    Für die `drums.yml` gibt es aus diesem Grund kein Schema.
