# Alias Queries

Der Alias-Query ist das Command-Argument welches angibt, mit welchen Aliasen der Sub-Command ausgeführt werden soll.

```shell title="Drush Command drush:multisite"
$ drush drush:multisite {==<alias-query>==} <sub-command> <sub-cmd-arguments>

# Kurzform
$ drush ms {==<alias-query>==} <sub-command> <sub-cmd-arguments>
```

Der Alias-Query **beginnt immer** mit einem **doppelten @-Zeichen**. Wird kein Alias-Query angegeben, so wird **```@@local``` als Default-Value** verwendet.

## Standard-Werte

**``@@local`` (default)** - Führt den Subcommand für alle lokalen Websites aus, d.h. für alle Seiten, die in dem Environment, in dem der Drush command eingegeben und ausgeführt wird. Wird kein Alias-Query eingegeben, so wird dieser Alias-Query als default-Wert verwendet.

**``@@remote``** - Führt den Subcommand für alle entfernten Websites aus, d.h. für alle Seiten, die NICHT im lokalen, sondern **entfernten Environments** liegen und nur per SSH erreicht werden können.

## Wildcard-Queries

Drush-Aliasse bestehen aus 1-3 Ids, zusammengesetzt aus diesen Zeichen: ```a-z A-Z 0-9 - _``` und mit dem Punkt ```.``` voneinander getrennt.

```shell
# 1-stellige Aliasse, typisch für Drupal Single-Site-Installationen.
@<Environment-ID>

# 2-stellige Aliasse, typisch für Drupal Multi-Site-Installationen.
@<Site-ID>.<Environment-ID>

# 3-stellige Aliasse, typisch für speziellen Drupal-Provider-Support.
@<Provider-ID>.<Site-ID>.<Environment-ID>
```

In einem **Alias-Query** kann jede ID (als ganze) durch eine **Wildcard ```*```** ersetzt werden, die für einen **beliebigen Wert** steht.

| Alias-Query   | Treffer                                                                  |
|:--------------|:-------------------------------------------------------------------------|
| `@@*`         | Wählt alle 1-stelligen Aliasse aus.                                      |
| `@@*.*`       | Wählt alle 2-stelligen Aliasse aus.                                      |
| `@@*.*.*`     | Wählt alle 3-stelligen Aliasse aus.                                      |
| `@@*.dev`     | Wählt alle 2-stelligen Aliasse im dev-Environment aus.                   |
| `@@*.stage`   | Wählt alle 2-stelligen Aliasse im stage-Environment aus.                 |
| `@@my-site.*` | Wählt alle 2-st. Aliasse der Website my-site auf allen Environments aus. |

Beispiel für einen DRUMS Command:

```shell
# Cache von allen Websites auf dem Stage-Environment löschen.
$ drush ms @@*.stage cr
```

!!!info "Liste meiner verfügbaren Aliasse anzeigen."

    Um eine Liste aller verfügbaren Aliase in Ihrer Drupal-Installation zu erhalten, können Sie den folgenden Command verwenden:

    ```
    $ drush site:alias --format=list
    ```


## Source-Target-Queries

Drush Commands, die zum Austausch von Daten dienen (z.B. sql:sync, config:pull), verwenden zwei Aliasse als Argument: Source und Target.

In einem DRUMS Alias-Query werden die zwei Argumente in einem Ausdruck zusammengefasst. Für Provider-ID und die Site-ID können Wildcards `*` eingesetzt werden. Für die Environments werden zwei Werte durch ein Hash-Zeichen `#` getrennt angegeben. Dabei steht der erste Wert für das Source-Environment, der zweite für das Target-Environment. Beide Werte müssen konkret angegeben werden, d.h. der Einsatz von Wildcards ist an dieser Stelle nicht möglich.

```shell
@@[<Provider-ID>|*.]<Site-ID>|*.{==<Source-Env-ID>#<Target-Env-ID>==}

# Für alle Websites, DB von prod nach dev synchronisieren.
$ drush ms @@*.prod#dev sql:sync
```

Der Drush Command wird für alle Source-Target-Alias-Paare ausgeführt, bei denen der erste Teil (zusammengesetzt aus Provider-ID und Site-ID) übereinstimmt und sowohl ein Source- als auch Target-Environment verfügbar ist.

!!! warning "Warnung vor Datenverlust 😰"

    Bitte vergewissern Sie sich vor der Ausführung dieses Commands, dass sie **alle Daten redundant gesichert** haben. Prüfen Sie eingehend die Trefferliste vor dem Bestätigen des Commands. Stellen Sie sicher, dass sie **nicht Source und Target verwechselt** haben.

## Alias-Query Pfad-Erweiterung

Drush Commands, die zusätzlich eine Angabe von Pfaden benötigen (z.B. core:rsync) können den Pfad-Parameter mit dem Colon-Zeichen `:` getrennt an den Query anhängen. Im Prinzip ist es möglich, hier einen absoluten Pfad bis zum Zielverzeichnis anzugeben; da jedoch alle Websites individuelle Pfade haben (z.B. den Files-Folder), ergäbe das keinen Sinn und würde zur Vermengung der Daten führen.

Bevor man dieses Feature also sinnvoll nutzen kann, müssen in jeder Alias-Config Pfade eingegeben werden, die als root für die Pfad-Angabe verwendet werden sollen: z.B. Files-, Config-Sync-, Private- und Backup-Folder.

```yaml title="Edit your Alias-Files: <root>/drush/sites/my-site.site.yml"
dev:
    uri: 'https://my-site.ddev.site/'
    paths:
      site: /var/www/html/docroot/sites/my-site
      files: /var/www/html/docroot/sites/my-site/files
      config: /var/www/html/docroot/sites/my-site/config
      private: /var/www/html/private/my-site
      backup: /var/www/html/backup/my-site
prod:
    host: www42.my-provider.de
    user: memyself
    root: /usr/home/memyself/public/prod/docroot
    uri: 'https://my-site.net'
    ssh: { options: '-p 222' }
    paths:
      drush-script: /usr/home/memyself/public/prod/bin/drush
      site: /usr/home/memyself/public/prod/docroot/sites/my-site
      files: /usr/home/memyself/public/prod/docroot/sites/my-site/files
      config: /usr/home/memyself/public/prod/docroot/sites/my-site/config
      private: /usr/home/memyself/public/prod/private/my-site
      backup: /usr/home/memyself/public/prod/backup/my-site
stage:
    # ...
```

Mit solchen Pfad-Angaben in der Alias-Definition können nun sinnvoll Verzeichnisse synchronisiert werden.

```shell
@@<Site-ID>|*.<Src-Env-ID>#<Tgt-Env-ID>{==:%<Path-ID>/folder/to/sync/==}

# Beispiel: Sync config folders von prod- nach dev-Env.
$ drush ms @@*.prod#dev:%config/sync/ core:rsync
```

!!! warning "Slash am Ende des Pfades beachten!"

    Bei dem rsync-Befehl macht ein **Slash am Ende des Source-Pfades** einen großen Unterschied aus (während dieser für gewöhnlich - so auch bei Target-Pfad - keinen Unterschied macht). Wird der Slash **nicht** gesetzt, so ist der **Ordner selbst** die Source (d.h. 1 Ordner). Wird er gesetzt, so ist der **Inhalt des Ordners** die Source (d.h. viele Ordner und Dateien).

    **Meistens ist die Angabe mit Slash die richtige Option**, da andernfalls - im Beispiel oben - im Ordner `config/sync` des Targets ein neuer Ordner mit Pfad `config/sync/sync` angelegt werden würde.
