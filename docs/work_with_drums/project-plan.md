# Konzept zur Nutzung von Drupal-Multisite

Eine Drupal-Multisite aufzusetzen wirft am Anfang viele Fragen auf, z.B. wie das Setup mit Composer und GIT am besten umzusetzen ist.

- Sollen die Sites-Folder ```/<docroot>/sites/site_a, .../site_b, .../site_c``` **in das Drupal-Projekt-Repo integriert** werden, oder eigene GIT-Repos bekommen?
- Wenn Sie eigene GIT-Repos bekommen, sollen diese dann **als Dependency** in das Projekt-Repository eingetragen werden?
- **Wie aktualisiert man eine Site** im Drupal-Multisite Kontext, ohne die anderen Sites zu beeinflussen oder zu gefährden?

Im Folgenden sollen diese Fragen beantwortet - und EINE Methode (bzw. Struktur) gezeigt werden, die sich vielfach bewährt hat und von DRUMS optimal unterstützt wird.

!!! note "Voraussetzungen"

    Im Folgenden gehen wir davon aus, dass Sie wissen, wie eine Drupal-Multisite als solche aufgebaut ist, was die ``sites.php`` ist, wo die Sites-Folder, die Drush-Aliasse usw. hingehören, und wie alles zusammenspielt. - Wenn Sie dazu noch Fragen haben, lesen Sie zunächst die [Dokumentation zu Drupal-Multisite](https://www.drupal.org/docs/getting-started/multisite-drupal).

## Composer Setup

Unsere Setup-Struktur in a Nutshell:

1. Richten Sie für jeden Sites-Folder ein eigenes GIT-Repo ein.
    1. Fügen Sie im Root des Repos eine [Datei ``drums.yml``](/work_with_drums/drums_yml_file/) ein und committen diese.
2. Richten Sie für das Drupal-Hauptprojekt ein GIT-Repo ein.
    1. Tragen Sie NICHT die Site-Repos als Dependency in das Haupt-Repo ein.
    2. Fügen Sie folgende Einträge zur ``.gitignore`` im Project-Root hinzu:
        ```gitignore title=".gitignore"
        # Sites folder
        docroot/sites/*/
        !docroot/sites/default/
        docroot/sites/sites.php

        # Drush aliasses
        drush/sites/*
        ```
3. Installieren Sie jeden Site-Folder mit ``git clone`` im Sites-Folder.
    ```
    $ cd docroot/sites
    $ git clone git@github.com/xxxxxxx/sites/my-site.git
    $ git clone git@github.com/xxxxxxx/sites/your-site.git
    ```


### Das Drupal-Projekt-Repo

Das Haupt-Repo sollte nur **gut gewartete Software-Pakete zusammenstellen**, die von den Sites (optional) genutzt werden können. Neben Drupal, Drush und DUMS sollte es alle Contrib-Module enthalten, die sie immer verwenden, wenn Sie mit Drupal arbeiten, z.B.: Admin Toolbar, Paragraphs, Webforms, COOKiES, SMTP, usw. (Empfehlung: [Environment-Indicator](https://www.drupal.org/project/environment_indicator))

**Redundanz vermeiden:** Wenn Sie Custom-Module oder -Themes auf mehr als einer Site verwenden, gehören diese ebenfalls in das Haupt-Repo, weil Sie diese andernfalls ja an mehreren Orten warten und aktualisieren müssten.

Vielleicht ist es eine gute Idee, eine **Drupal Distribution** (wie z.B: Thunder CMS) zu verwenden, oder ein **eigenes Installations-Profil** einzurichten, mit dem sie **neue Sites im Multisite-Kontext** erstellen.

??? quote "Halten sie alle Site-spezifische Config aus dem Haupt-Repo heraus!"

    **Ignorieren Sie mit ``.gitignore``** die Site-Ordner im Drupal-Sites-Folder, die `sites.php`, die Site-Aliasse, schlicht alles, was Site-spezifisch ist. So dass Sie beliebig viele Sites hinzufügen und entfernen können, ohne eine einzige GIT-Status-Änderung im Drupal-Haupt-Repo.

### DRUMS Setup

DRUMS unterstützt mit automatisierten Prozessen, eine Site in der Drupal-Umgebung lauffähig zu machen. Fügen Sie in jeden Site-Folder eine [Datei ``drums.yml``](/work_with_drums/drums_yml_file/) ein, welche alle dazu nötigen Informationen enthält.

Führen Sie nach dem Hinzufügen oder Entfernen einer Site, oder wenn Sie Informationen in einer ``drums.yml`` aktualisiert haben, den folgenden Command aus:

```shell
$ drush drums:setup
```

Out-of-The-Box wird DRUMS

1. **die Site-Alliasse** im Ordner ``drush/sites/`` erzeugen/aktualisieren
2. **die ``sites.php``** im Ordner ``docroot/sites/`` erzeugen/aktualisieren

Für individuelle Anpassungen Ihres Setups, können Sie den Drush-Hook ``drums-setup-services`` nutzen, z.B. um spezielle Dateien und Verzeichnisse zu generieren, oder eine Datenbank bereitzustellen. Lesen Sie dazu die [Dokumentation zum Command ``drums:setup``](/commands/drums_setup/).

### Empfehlung

Richten Sie alles so ein, dass sich eine neue Site auf einem Environment möglichst autark verhalten kann. Nutzen Sie z.B. die ``.env`` um für jedes Environment individuell z.B. die Datenbank-Verbindung anzugeben, und um Environment spezifische Informationen festzulegen.

```php title="docroot/sites/my-site/settings.php"
<?php
# ...

$databases['default']['default'] = [
    'host' => getenv('DB_HOST') ?? 'localhost',
    'database' => 'my-site-db',
    'username' => getenv('DB_USERNAME'),
    'password' => getenv('DB_PASSWORD') ?? '',
    'prefix' => '',
    'port' => getenv('DB_PORT') ?? 3306,
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
];


```
