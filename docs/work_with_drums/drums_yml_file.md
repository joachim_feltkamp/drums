# Die ```drums.yml```-Datei

```yaml title="$docroot/sites/my_site/drums.yml"
siteid: my_site
enabled: 1
ddev_host: my-site
drushalias:
  ddev:
    uri: https://my-site.ddev.site/
    paths:
      site: '/var/www/html/web/sites/my_site'
      files: '/var/www/html/web/sites/my_site/files'
      private: '/var/www/html/web/sites/my_site/private'
      config: '/var/www/html/web/sites/my_site/config'
  stage:
    host: www42.my-provider.de
    user: memyself
    root: /usr/home/memyself/web/staging/web
    uri: https://stage.my-site.de
    ssh:
      options: '-p 222'
    paths:
      drush-script: '/usr/home/memyself/web/staging/web/staging/bin/drush'
      site: '/usr/home/memyself/web/staging/web/sites/my_site'
      files: '/usr/home/memyself/web/staging/web/sites/my_site/files'
      private: '/usr/home/memyself/web/staging/web/sites/my_site/private'
      config: '/usr/home/memyself/web/staging/web/sites/my_site/config'
  prod:
    host: www42.my-provider.de
    user: memyself
    root: /usr/home/memyself/web/production/web
    uri: https://my-site.de
    ssh:
      options: '-p 222'
    paths:
      drush-script: '/usr/home/memyself/web/production/web/production/bin/drush'
      site: '/usr/home/memyself/web/production/web/sites/my_site'
      files: '/usr/home/memyself/web/production/web/sites/my_site/files'
      private: '/usr/home/memyself/web/production/web/sites/my_site/private'
      config: '/usr/home/memyself/web/production/web/sites/my_site/config'
vhosts:
  - 'my-site.ddev.site'
  - 'stage.my-site.de'
  - 'my-site.de'
```
