
# Drums - Drupal Multi-Site

Scripte und Processe, um das Datei-System einer Drupal Multisite-
installation zu managen.

## Requirements
- drupal ^8 || ^9
- drush ^9 || ^10
- git ^2.18
- tar

## Edit MkDocs documentation

Requires python and pip.
Read: https://docs.readthedocs.io/en/stable/intro/getting-started-with-mkdocs.html

```
// Install mkdocs
$ pip install mkdocs

// You might export the path. Try ...
$ pip --version
$ pip3 --version

// Will output smth. like this.
pip 22.3.1 from /Users/<username>/Library/Python/3.9/lib/python/site-packages/pip (python 3.9)

// And then ...
export PATH=/Users/<username>/Library/Python/3.9/bin:$PATH

// Run dev server and edit.
$ mkdocs serve
```

