<?php

namespace Drums;

use DrupalFinder\DrupalFinder;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DrumsFileManager {

  /**
   * @var \DrupalFinder\DrupalFinder|null
   */
  private ?DrupalFinder $drupalFinder;

  /**
   * @var Filesystem|null
   */
  private ?Filesystem $fs;

  /**
   * @var string
   */
  private string $composerRoot;

  /**
   * @var string
   */
  private string $drupalRoot;

  public function __construct() {
    $this->fs = new Filesystem();
    $this->drupalFinder = new DrupalFinder();
    $this->drupalFinder->locateRoot(getcwd());
    $this->composerRoot = $this->drupalFinder->getComposerRoot();
    $this->drupalRoot = $this->drupalFinder->getDrupalRoot();
  }

  /**
   * @param $uri
   * @param string $content
   * @param array|null $post_cmd
   * @param bool $is_drupal_root
   *
   * @return array
   *
   * @throws \Symfony\Component\Filesystem\Exception\IOException
   *
   * @ToDo Give feedback as ids for CREATED, UPDATED, DELETED, SKIPPED.
   */
  public function setFileContent($uri, string $content, ?array $post_cmd = [], bool $is_drupal_root = FALSE) {
    $basePath = ($is_drupal_root) ? $this->drupalRoot : $this->composerRoot;
    $file_path = "{$basePath}/{$uri}";
    $status = $this->fs->exists($file_path) ? 'check': 'create';
    if ($status == 'check') {
      $prev_content = file_get_contents($file_path);
      if ($content != $prev_content) {
        $this->fs->remove($file_path);
        $status = 'update';
      } else {
        $status = 'skip';
      }
    }
    $status = ($status == 'skip' || !!file_put_contents($file_path, $content))
      ? $status : 'error';
    return [
      'uri' => ($is_drupal_root) ? "(drupal_root)/$uri" : $uri,
      'status' => $status,
      'post_cmd' => $post_cmd[$status] ?? NULL,
    ];
  }

  public function cloneFile($source_uri, $target_uri, bool $is_drupal_root = FALSE) {
    if ($this->fs->exists($source_uri)) {
      return $this->setFileContent($target_uri, file_get_contents($source_uri));
    }
  }

  public function removeFile($file_uri, $post_cmd = [], bool $is_drupal_root = FALSE) {
    $basePath = ($is_drupal_root) ? $this->drupalRoot : $this->composerRoot;
    $file_path = "$basePath/$file_uri";
    if ($this->fs->exists($file_path)) {
      $this->fs->remove($file_path);
      return [
        'uri' => $file_path,
        'status' => 'delete',
        'post_cmd' => $post_cmd['delete'] ?? NULL,
      ];
    } else {
      return [
        'uri' => $file_path,
        'status' => 'error',
        'post_cmd' => $post_cmd['error'] ?? NULL,
      ];
    }
  }

  /**
   * @param $folder
   *   Folder without trailing slash.
   * @param bool $is_drupal_root
   *   Is folder in Drupal root (TRUE) or in composer root (FALSE: default)?
   *
   * @return void
   */
  public function prepareFolder($folder, bool $is_drupal_root = FALSE) {
    $basePath = ($is_drupal_root) ? $this->drupalRoot : $this->composerRoot;
    if (!$this->fs->exists($basePath.$folder)) $this->fs->mkdir($folder);
  }

  /**
   * @param string $folder
   *   Folder where to search in.
   * @param string $query
   *   Files name or regEx for files.
   * @param array|int|string $depth
   *   Depth of query
   * @param bool $is_drupal_root
   *
   *
   * @return \Symfony\Component\Finder\Finder
   */
  public function findFiles(string $folder, string $query, array|int|string $depth = '>= 0', bool $is_drupal_root = FALSE) {
    $basePath = ($is_drupal_root) ? $this->drupalRoot : $this->composerRoot;
    $finder = new Finder();
    return $finder->depth($depth)->files()->name($query)->in("$basePath/$folder");
  }
}
