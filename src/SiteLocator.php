<?php

namespace Drums;

use Consolidation\SiteAlias\SiteAlias;
use Consolidation\SiteAlias\SiteAliasManager;
use DrupalFinder\DrupalFinder;
use Drush\Drush;
use Drush\SiteAlias\SiteAliasFileLoader;

class SiteLocator {

  /**
   * @var \DrupalFinder\DrupalFinder|null
   */
  private ?DrupalFinder $drupalFinder;


  /**
   * @var \Consolidation\SiteAlias\SiteAliasManager|null
   */
  private ?SiteAliasManager $siteAliasManager;

  /**
   * @var \Consolidation\SiteAlias\SiteAliasInterface[]
   */
  private array $siteAliases = [];


  public function __construct()
  {
    $this->drupalFinder = new DrupalFinder();
    $this->drupalFinder->locateRoot(getcwd());

    $this->siteAliasManager = new SiteAliasManager(new SiteAliasFileLoader());
    $this->siteAliasManager->addSearchLocation($this->drupalFinder->getComposerRoot() . '/drush/sites');
  }


  /**
   * Getter for site aliases.
   *
   * @return \Consolidation\SiteAlias\SiteAliasInterface[]
   */
  public function getSiteAliases(): array
  {
    if (!count($this->siteAliases)) {
      $this->siteAliases = $this->siteAliasManager->getMultiple();
    }
    return $this->siteAliases;
  }

  /**
   * Get a list of site directory names for a site group.
   *
   * @return array
   *   Contents of the $sites variable.
   */
  public function getSiteDirNames(): array
  {
    if (!$sitesFile = $this->getSitesFile()) {
      return [];
    }
    return $sitesFile->getDirNames();
  }

  /**
   * Get site root folder.
   *
   * @param \Consolidation\SiteAlias\SiteAlias $alias
   *
   * @return string|null
   *
   * @Todo Find propper way to get site folder without firing drush status command.
   */
  public function getSiteRoot(SiteAlias $alias) {
    $site_root = $alias->get('site');
    if (!$site_root) {
      $process = $this->processManager()->drush($alias, 'status', [], ['format' => 'json']);
      $status_info = $process->mustRun()->getOutputAsJson();
      $site_root = $status_info['site'] ?? "site/{$alias->name()}";
      Drush::bootstrapManager()->drupalFinder();

    }
    return $site_root;
  }


  private function getSitesFile(): ?SitesFile {
    if (!$drupalRoot = $this->drupalFinder->getDrupalRoot()) {
      return NULL;
    }

    return new SitesFile("$drupalRoot/sites/sites.php");
  }

}
