<?php

namespace Drums;


use Consolidation\SiteAlias\SiteAliasManager;
use Consolidation\SiteAlias\SiteAliasManagerAwareInterface;
use Consolidation\SiteAlias\SiteAliasManagerAwareTrait;
use Drums\aliases\DrumsAliasSingleSite;
use Drums\aliases\DrumsAliasSourceTarget;
use DrupalFinder\DrupalFinder;
use Drush\SiteAlias\SiteAliasFileLoader;
use Drush\Style\DrushStyle;
use Exception;

class DrumsAliasCollection implements \IteratorAggregate, SiteAliasManagerAwareInterface {

  use SiteAliasManagerAwareTrait;

  /**
   * @var string
   */
  private string $type = '';

  /**
   * @var \DrupalFinder\DrupalFinder|null
   */
  private ?DrupalFinder $drupalFinder;

  /**
   * @var \Consolidation\SiteAlias\SiteAliasInterface[]
   */
  private array $siteAliases = [];

  /**
   * @var \Drums\aliases\DrumsAliasItemInterface[]
   */
  private array $drumsAliasItems = [];

  /**
   * @var DrushStyle
   */
  protected DrushStyle $io;

  /**
   * @var string
   */
  const SINGLE_SITE = 'single_site';

  /**
   * @var string
   */
  const SOURCE_TARGET = 'source_target';

  /**
   * @var string
   */
  const INVALID_QUERY = 'invalid_query';

  /**
   * Regular expression pattern for valid alias query.
   * See: https://regex101.com/r/YOV8iG/1
   * BASE_ENV: $1 // HOST_EXT: $3 // TARGET_EXT: $5 // PATH: $6
   *
   * @var string
   */
  const ALIAS_QUERY_PATTERN = '/^@@((\*\.|[\w\-]+\.){0,2})(\*|[\w\-]+)(#([\w\-]+))?(:%?([\/\w\- %]+)(\.\w{1,8})?)?$/';

  /**
   * @param $query
   *
   * @throws \Exception
   */
  public function __construct($query, $io) {
    $this->drupalFinder = new DrupalFinder();
    $this->drupalFinder->locateRoot(getcwd());

    $this->siteAliasManager = new SiteAliasManager(new SiteAliasFileLoader());
    $this->siteAliasManager->addSearchLocation($this->drupalFinder->getComposerRoot() . '/drush/sites');
    $this->io = $io;
    $this->aliasQuery($query);
  }

  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }


  #[\ReturnTypeWillChange]
  public function getIterator() {
    return new \ArrayIterator($this->drumsAliasItems);
  }

  /**
   * @return int
   */
  public function count(): int {
    return count($this->drumsAliasItems);
  }

  /**
   * @param array $options
   *
   * @return array
   */
  public function getTable(array $options = []): array {
    $table_head = ($this->type == self::SOURCE_TARGET)
      ? ['source' => '@_SOURCE', 'direction' => '<fg=cyan>=></>', 'target' => '@_TARGET']
      : ['alias' => '@_ALIAS', 'env' => 'Env'];
    $type = $options['type'] ?? '';
    if ($type == 'wd') {
      $table_head['wd'] = '@_CWD';
    }
    $table_body = [];
    foreach ($this as $drums_alias_item) {
      $table_body[] = $drums_alias_item->getTableRow($options);
    }
    return [$table_head, $table_body];
  }

  /**
   * Query alias by @@ wildcard notation.
   *
   * @param ?string $q
   *   The query string with wildcards included (e.g. @@mysite.* or @@*.dev).
   *
   * @return void
   * @throws \Exception
   *
   * @ToDo @@local:%files/img/ is not working.
   */
  public function aliasQuery(?string $q = '@@local'): void
  {
    $all_aliases = $this->siteAliasManager->getMultiple();
    switch($q) {
      case '@@local':
        $this->type = self::SINGLE_SITE;
        foreach($all_aliases as $alias) {
          if ($alias->isLocal()) {
            $this->drumsAliasItems[] = new DrumsAliasSingleSite($alias);
          }
        }
        break;
      case '@@remote':
        $this->type = self::SINGLE_SITE;
        foreach($all_aliases as $alias) {
          if ($alias->isRemote()) {
            $this->drumsAliasItems[] = new DrumsAliasSingleSite($alias);
          }
        }
        break;
      default:
        $query_analysis = $this->validateQuery($q);
        if ($this->type == self::SOURCE_TARGET) {
          // Get a source-target query @@*.ddev:prod
          $all_aliases_keys = array_keys($all_aliases);
          $site_names = array_unique(array_map(function($alias_name) use ($query_analysis) {
            preg_match($query_analysis['base_tester'], $alias_name, $alias_name_matches);
            return $alias_name_matches[0] ?? NULL;
          }, $all_aliases_keys));
          foreach ($site_names as $site_name) {
            $source = "$site_name{$query_analysis['source_site']}";
            $target = "$site_name{$query_analysis['target_site']}";
            if (in_array($source, $all_aliases_keys) && in_array($target, $all_aliases_keys)) {
              $this->drumsAliasItems[] = new DrumsAliasSourceTarget($all_aliases[$source], $all_aliases[$target], $query_analysis['file_path']);
            }
          }
        }
        elseif ($this->type == self::SINGLE_SITE) {
          foreach($all_aliases as $alias) {
            if (preg_match($query_analysis['site_tester'], $alias->name())) {
              $this->drumsAliasItems[] = new DrumsAliasSingleSite($alias, $query_analysis['file_path']);
            }
          }
        }
      // End default
    }

    if (!$this->count() || $this->type == self::INVALID_QUERY) {
      $this->io->writeln("See following list of <fg=cyan>valid queries</>:");
      $this->io->table(
        ['Queries', 'Description'],
        $this->getValidQueries(),
      );
      if ($this->type == self::INVALID_QUERY) {
        throw new \Exception("Drums alias query '$q' seems to be invalid."
          . "Use schema: @@[farm/name][.site/name (optional)][.name (optional)]".
          "[#target_name (optional)][:file_path (optional)]");
      } else {
        throw new \Exception("Drums alias query '$q' has no result.");
      }
    }
  }

  /**
   * Validate query.
   *
   * @param ?string $q
   *   The query string with wildcards included (e.g. @@mysite.* or @@*.dev).
   *
   * @return array
   * @throws \Exception
   */
  public function validateQuery(?string $q): array
  {
    if (preg_match(self::ALIAS_QUERY_PATTERN, $q, $matches)) {
      $this->type = ($matches[5] ?? NULL) ? self::SOURCE_TARGET : self::SINGLE_SITE;

      $source = "@@{$matches[1]}{$matches[3]}";

      if ($this->type == self::SOURCE_TARGET) {
        if (!preg_match('/^[\w-]+$/', $matches[3])) {
          $this->type = self::INVALID_QUERY;
          $this->io->error("Source site must be a concrete value. A wildcard (*) is not allowed here: @@$matches[1]==>$matches[3]<==#$matches[5]");
          return [];
        }
        $target = "@@$matches[1]$matches[5]";
        return [
          "base" => "@@$matches[1]",
          "base_tester" => $this->getTester("@@$matches[1]", TRUE, FALSE),
          "source" => $source,
          "source_site" => $matches[3],
          "target" => $target,
          "target_site" => $matches[5],
          "file_path" => $matches[6] ?? null
        ];
      }
      elseif ($this->type == self::SINGLE_SITE) {
        return [
          "site" => $source,
          "site_tester" => $this->getTester($source),
          "file_path" => $matches[6] ?? null
        ];
      }
    }
    $this->type = self::INVALID_QUERY;
    return [];

  }


  /**
   * Get a valid regular expression from an alias query.
   *
   * @param string $query
   *   Get a regular expression from a simple query string.
   *   E.g. @@*.my-site.dev => /^@([\w-]+)\.my-site\.dev$/
   *   matches @env-a.my-site.dev, @env-b.my-site.dev, ...
   * @param bool $bos
   *   Add carrot at begin of the pattern.
   * @param bool $eos
   *   Add dollar sign at end of the pattern.
   *
   * @return string
   *   RegExpression to test an alias string.
   */
  protected function getTester(string $query, bool $bos = TRUE, bool $eos = TRUE): string
  {
    $base_pattern = str_replace(['@@', '.', '*'], ['@', '\.', '([\w-]+)'], $query);
    $begin = $bos ? '^' : '';
    $end = $eos ? '$' : '';
    return "/{$begin}$base_pattern{$end}/";
  }


  /**
   * @return array|array[]
   */
  public function getValidQueries() {
    $all_aliases = $this->siteAliasManager->getMultiple();
    $vq = (count($all_aliases)) ? [
      ['query' => '@@local', 'desc' => 'Hit aliases hosted local.'],
      ['query' => '@@remote', 'desc' => 'Hit aliases hosted remote.'],
      ['query' => '@@* | @@*.* | @@*.*.*', 'desc' => 'Hit all aliases with wildcard schema.'],
    ] : [];
    $sites = [];
    $envs = [];
    foreach ($all_aliases as $key => $alias) {
      preg_match("/^@([\w-]+)\.([\w-]+)$/", $key, $matches);
      $sites[] = $matches[1];
      $envs[] = $matches[2];
    }
    if (count($sites)) {
      $sites = array_map(function($s) {return "@@$s.*";}, $sites);
      $vq[] = [
        'query' => join("\n" , array_unique($sites)),
        'desc' => 'Hit all aliases of one site on different environments.'
      ];
    }
    if (count($envs)) {
      $envs_str = array_map(function($e) {return "@@*.$e";}, $envs);
      $vq[] = [
        'query' => join("\n", array_unique($envs_str)),
        'desc' => 'Hit all different sites on a single environment.'
      ];
    }
    if (count($envs) >= 2) {
      $sync_alias = "@@*.$envs[1]<fg=cyan>#</>$envs[0]";
      $vq[] = [
        'query' => $sync_alias,
        'desc' => 'Alias-sync notation for source-target commands: e.g. sql:sync.'
      ];
      $vq[] = [
        'query' => "$sync_alias<fg=cyan>:%files/img/</>",
        'desc' => 'Notation for commands with folder reference: e.g. core:rsync.'
      ];
    }
    return $vq;
  }

}
