<?php

namespace Drums\aliases;

use Consolidation\SiteAlias\SiteAliasInterface;

interface DrumsAliasItemInterface {

  /**
   * Get the alias argument string
   *
   * @return string
   */
  public function getArgumentString(): string;

  /**
   * Get a row for info table.
   *
   * @param array $options
   *
   * @return array
   */
  public function getTableRow(array $options = []): array;

  /**
   * Get a row for info table.
   *
   * @param string|NULL $ref
   *
   * @return \Consolidation\SiteAlias\SiteAliasInterface|\Consolidation\SiteAlias\SiteAliasInterface[]
   */
  public function getAlias(string $ref = null): SiteAliasInterface|array;

  /**
   * @param string|null $default_cwd
   *   Default path to use if query has no path defined. (Standard: 'site').
   *
   * @return string|null
   *
   * @throws \Exception
   */
  public function getCwd(string $default_cwd = NULL): string|null;
}
