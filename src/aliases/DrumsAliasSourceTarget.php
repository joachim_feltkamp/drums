<?php

namespace Drums\aliases;

use Consolidation\SiteAlias\SiteAliasInterface;

class DrumsAliasSourceTarget implements DrumsAliasItemInterface {

  /**
   * @var SiteAliasInterface
   */
  private SiteAliasInterface $sourceAlias;

  /**
   * @var SiteAliasInterface
   */
  private SiteAliasInterface $targetAlias;

  /**
   * @var string|NULL
   */
  private string|NULL $filePath;

  public function __construct(SiteAliasInterface $source_alias, SiteAliasInterface $target_alias, string $file_path = null) {
    $this->sourceAlias = $source_alias;
    $this->targetAlias = $target_alias;
    $this->filePath = $file_path;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentString(): string {
    return "{$this->sourceAlias->name()}$this->filePath {$this->targetAlias->name()}$this->filePath";
  }

  /**
   * {@inheritdoc}
   */
  public function getTableRow(array $options = []): array {
    return [
      'source' => $this->sourceAlias->name().$this->filePath,
      'direction' => '<fg=cyan>=></>',
      'target' => $this->targetAlias->name().$this->filePath,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getAlias(string $ref = null): SiteAliasInterface|array {
    return match ($ref) {
      'source' => $this->sourceAlias,
      'target' => $this->targetAlias,
      default => [
        'source' => $this->sourceAlias,
        'target' => $this->targetAlias
      ],
    };

  }

  /**
   * {@inheritdoc}
   */
  public function getCwd(string $default_cwd = NULL): string|null {
    return $this->filePath ?? $default_cwd;
  }
}
