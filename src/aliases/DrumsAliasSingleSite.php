<?php

namespace Drums\aliases;

use Consolidation\SiteAlias\SiteAliasInterface;

class DrumsAliasSingleSite implements DrumsAliasItemInterface {

  /**
   * @var SiteAliasInterface
   */
  private SiteAliasInterface $siteAlias;

  /**
   * @var string|NULL
   */
  private string|NULL $filePath;

  public function __construct(SiteAliasInterface $site_alias, string $file_path = null) {
    $this->siteAlias = $site_alias;
    $this->filePath = $file_path;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentString(): string {
    return $this->getAlias()->name();
  }

  /**
   * {@inheritdoc}
   */
  public function getTableRow(array $options = []): array {
    $row = [
      'alias' => $this->getAlias()->name(),
      'env' => $this->getAlias()->isLocal() ? 'Local' : '<fg=yellow>Remote</>',
    ];
    $type = $options['type'] ?? '';
    if ($type == 'wd') {
      $default_cwd = $options['default_cwd'] ?? '';
      $row['wd'] = ($this->filePath) ? substr($this->filePath, 1) : $default_cwd;
    }
    return $row;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlias(string $ref = null): SiteAliasInterface|array {
    return $this->siteAlias;
  }

  /**
   * {@inheritdoc}
   */
  public function getCwd(string $default_cwd = NULL): string|null {
    if ($this->filePath && strlen($this->filePath) >= 2) {
      if (preg_match('/^:%([\w-]+)(.*)/', $this->filePath, $matches)) {
        if ($alias_path = $this->getAlias()->get("paths.$matches[1]")) {
          $ext = $matches[2] ?? '';
          return $alias_path.$ext;
        } else {
          throw new \Exception("Path '$matches[1]' is not defined in alias {$this->getAlias()->name()}.");
        }
      } else {
        return substr($this->filePath, 1);
      }
    } elseif($default_cwd) {
      return $this->getAlias()->get("paths.$default_cwd");
    }

    return NULL;
  }
}
